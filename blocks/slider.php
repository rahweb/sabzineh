<div class="slider">
    <div id="demo" class="carousel slide" data-ride="carousel">
    <!-- <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul> -->
    <div class="carousel-inner">
        <div class="carousel-item active">
        <img src="images/slider/AWC_Sprayer.png" alt="Chicago" width="1100" height="500">

            <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
            </div>   
        </div>
        <div class="carousel-item">
        <img src="images/slider/VIC_ixtrack-T_0013-2000-x-500px-photoshopped-and-cropped-2000x500.jpg" alt="Los Angeles" width="1100" height="500">
            <div class="carousel-caption">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
            </div>   
        </div>
        <div class="carousel-item">
            <img src="images/slider/grapes-2759991-2000x500.jpg" alt="New York" width="1100" height="500">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>   
        </div>
    </div>
    <!-- <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a> -->
    </div>
</div>