<!doctype html>
<html lang="en">
  <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/menu.php");?>
    <div class="row img">
        <img src="images/DVS_services_RGB-2000x300.jpg" alt="">
    </div>
    <div class="brad">
        <br>
        <div class="container">
            <div class="row">
                <ul>
                    <li class="home"><a href="index.php">HOME</a></li>
                    <li><span class="flaticon flaticon-next"></span></li>
                    <li><a>INFORMATION</a></li>
                </ul>
            </div>
        </div>
    </div>
    <section id="contact" class="parallax-section">
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.2s">
                        <h3>Contact Us Information</h3>
                        <hr class="hr">
                        <p> We offer quick reply support via telephone and email, which guarantee that you can get response within 12 hours.</p>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="row social">
                                <div class="col-md-2">
                                    <svg aria-hidden="true" data-prefix="fas" data-icon="phone-volume" class="svg-inline--fa fa-phone-volume fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M97.333 506.966c-129.874-129.874-129.681-340.252 0-469.933 5.698-5.698 14.527-6.632 21.263-2.422l64.817 40.513a17.187 17.187 0 0 1 6.849 20.958l-32.408 81.021a17.188 17.188 0 0 1-17.669 10.719l-55.81-5.58c-21.051 58.261-20.612 122.471 0 179.515l55.811-5.581a17.188 17.188 0 0 1 17.669 10.719l32.408 81.022a17.188 17.188 0 0 1-6.849 20.958l-64.817 40.513a17.19 17.19 0 0 1-21.264-2.422zM247.126 95.473c11.832 20.047 11.832 45.008 0 65.055-3.95 6.693-13.108 7.959-18.718 2.581l-5.975-5.726c-3.911-3.748-4.793-9.622-2.261-14.41a32.063 32.063 0 0 0 0-29.945c-2.533-4.788-1.65-10.662 2.261-14.41l5.975-5.726c5.61-5.378 14.768-4.112 18.718 2.581zm91.787-91.187c60.14 71.604 60.092 175.882 0 247.428-4.474 5.327-12.53 5.746-17.552.933l-5.798-5.557c-4.56-4.371-4.977-11.529-.93-16.379 49.687-59.538 49.646-145.933 0-205.422-4.047-4.85-3.631-12.008.93-16.379l5.798-5.557c5.022-4.813 13.078-4.394 17.552.933zm-45.972 44.941c36.05 46.322 36.108 111.149 0 157.546-4.39 5.641-12.697 6.251-17.856 1.304l-5.818-5.579c-4.4-4.219-4.998-11.095-1.285-15.931 26.536-34.564 26.534-82.572 0-117.134-3.713-4.836-3.115-11.711 1.285-15.931l5.818-5.579c5.159-4.947 13.466-4.337 17.856 1.304z"></path></svg> 
                                </div>
                                <div class="col-md-10">
                                    <ul>
                                        <li>010-020-0340</li>
                                        <li>010-020-0340</li>
                                        <li>010-020-0340</li>
                                    </ul>  
                                </div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="row social">
                                <div class="col-md-2">
                                    <svg aria-hidden="true" data-prefix="fas" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg>
                                </div>
                                <div class="col-md-10">
                                    <ul>
                                        <li>info@company.com<span>(sales inquiries)</span></li>
                                        <li>info@company.com<span>(sales inquiries)</span></li>
                                        <li>info@company.com<span>(sales inquiries)</span></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="row social">
                                <div class="col-md-2">
                                    <svg aria-hidden="true" data-prefix="fas" data-icon="comment-alt" class="svg-inline--fa fa-comment-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z"></path></svg>
                                </div>
                                <div class="col-md-10">
                                    <ul>
                                        <li> Live Chat</li>
                                        <li>
                                            Chat online: with our sales representatives now. 
                                        </li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <br>
                    <br>
                    <div class="wow fadeInUp section-title" data-wow-delay="0.2s">
                        <h3>Contact Form</h3>
                        <hr class="hr">
                        <p>Connect using the map below</p>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.4s">
                        <form id="contact-form" action="#" method="get">
                            <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control" name="name" placeholder="Name" required="">
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="">
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control form-control-lg" name="category" id="validationCustom03" style="margin: 3% 0%;" onchange="ChangecatList()" required>
                                    <option value="">Choose... </option>
                                    <option value="Classroom Instruction and Assessment">Classroom Instruction and Assessment</option>
                                    <option value="Curriculum Development and Alignment">Curriculum Development and Alignment</option>
                                    <option value="District Committee">District Committee</option>
                                    <option value="Meeting">Meeting</option>
                                    <option value="Other Category">Other Category</option>
                                    <option value="Professional Conference">Professional Conference</option>
                                    <option value="Professional Workshop / Training">Professional Workshop / Training</option>
                                    <option value="Pupil Services">Pupil Services</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="email" class="form-control" name="email" placeholder="Telephone" required="">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <textarea class="form-control" rows="5" name="message" placeholder="Message" required=""></textarea>
                            </div>
                            <div class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6">
                                <button id="submit" type="submit" class="form-control" name="submit">Send Message</button>
                            </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <br>
                    <br>
                    <div class="wow fadeInUp section-title" data-wow-delay="0.2s">
                        <h3>Location</h3>
                        <hr class="hr">
                        <p>Find us on the map below</p>
                    </div>
                    <div id="contact" class="map" style="margin-top: 1%">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.3229616181357!2d51.35102471485428!3d35.71827498018567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e0751448451c9%3A0x24a41b077fd2a86a!2sKhaleh+Setareh+Language+Academy!5e0!3m2!1sen!2sua!4v1543843611497" style="border:0" allowfullscreen="" width="100%" height="400" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </section>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>