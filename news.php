<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="row img">
            <img src="images/yonetim-danismanligi-banner-2000x300.jpg" alt="">
        </div>
        <div class="brad">
            <br>
            <div class="container">
                <div class="row">
                    <ul>
                        <li class="home"><a href="index.php">HOME</a></li>
                        <li><span class="flaticon flaticon-next"></span></li>
                        <li><a>NEWS & BLOG</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="news">
            <br>
            <div class="container">
                <div class="row text" style="margin-bottom: 4px;">
                    <div class="col-md"></div>
                    <div class="col-md text-center">
                        <h3> News</h3>
                    </div>
                    <div class="col-md"></div>
                </div>
                <br>
                <br>
                <div class="row news-s">
                    <br>
                    <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row text" style="margin-bottom: 4px;">
                    <div class="col-md"></div>
                    <div class="col-md text-center">
                        <h3>Blog</h3>
                    </div>
                    <div class="col-md"></div>
                </div>
                <br>
                <br>
                <div class="row news-s">
                <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md col-sm-12 col-xs-12">
                            
                            <a href="news-details.php">
                                <div class="img">
                                    <img src="images/news/1000_stone_farm_135.jpg" alt="">
                                </div>
                                <br>
                                <br>
                                <div class="text-center">
                                    <h5>NEWS SAMPLE 1</h5>
                                    <br>
                                    <p>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    <br>
                                    TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
                                    </p>
                                </div>
                            </a>
                        </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>